class CreateIdcards < ActiveRecord::Migration
  def change
    create_table :idcards do |t|
    t.string :name
     t.string :designation
     t.string :idcard_type

      t.timestamps
    end
  end
end
