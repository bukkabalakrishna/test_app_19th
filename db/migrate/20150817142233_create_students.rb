class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :first_name
      t.string :last_name
      t.string :middle_name
      t.string :phone
       t.string :date_of_birth
      t.string :email
      t.text :address
      t.timestamps
    end
  end
end
