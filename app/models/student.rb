class Student < ActiveRecord::Base
  validates :first_name, :presence => true
  validates_numericality_of :phone,:allow_blank => true
  validates :email, :presence => true,
    :length => {:minimum => 3, :maximum => 254},
    #    :uniqueness => true,
  :format => {:with => /\A^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$\z/i}

 has_one :idcard
 belongs_to :college,:foreign_key => 'college_id'

end
