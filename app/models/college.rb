class College < ActiveRecord::Base
	validates :college_name, :address, :presence => true
	validates_numericality_of :college_code, :allow_blank => true

	has_many :students
end
