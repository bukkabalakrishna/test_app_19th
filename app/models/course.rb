class Course < ActiveRecord::Base
	validates :course_name, :description, :presence => true
	validates_numericality_of :course_code, :allow_blank => true
end
