class Idcard < ActiveRecord::Base
	validates :name, :designation, :idcard_type, :presence => true
	 belongs_to :student
end
