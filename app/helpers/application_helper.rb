module ApplicationHelper
	def validation_errors(message)
      if message.class.to_s == "Array"
      message = message.join(", ")
      end
     return !message.to_s.blank? ? raw("<div style='color: red;'>"+message.to_s+"</div>") : ""
    end
end
