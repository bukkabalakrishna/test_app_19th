class IdcardsController < ApplicationController
		def new
    @idcard = Idcard.new
  end

  def index
    @idcards = Idcard.all

  end

  def create
    @idcard = Idcard.new(idcard_params)
    if @idcard.save
      redirect_to idcards_path
    else
      render :action => 'new'
    end
  end
  def edit
    @idcard = Idcard.find(params[:id])
  end
  def update
    @idcard = Idcard.find(params[:id])
    if @idcard.update_attributes(idcard_params)
       redirect_to idcard_path
    else
       render :action => 'edit'
    end
  end

  def show
    @idcard = Idcard.find(params[:id])
  end

  def destroy
    @idcard = Idcard.find(params[:id])
    if @idcard.delete
      redirect_to idcards_path
    end

  end

  private

  def idcard_params
    params.require(:idcard).permit(:name,:designation,:idcard_type,:student_id  )
  end
end
