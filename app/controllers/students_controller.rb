class StudentsController < ApplicationController
  
  def index
  	@students = Student.all
  end
  
  def new
  	@student = Student.new
  end

  def create
    @student = Student.new(student_params)
    if @student.save
       redirect_to students_path
    else
       render :action => 'new'
    end
  end

  def edit
    @student = Student.find(params[:id])
  end
  def update
    @student = Student.find(params[:id])
    if @student.update_attributes(student_params)
       redirect_to student_path
    else
       render :action => 'edit'
    end
  end


  def show
  	@student = Student.find(params[:id])
  end

  def destroy
    @student = Student.find(params[:id])
    if @student.delete
       redirect_to students_path
    end
  end

  private

  def student_params
    params.require(:student).permit(:first_name,:last_name,:middle_name,:phone,:date_of_birth,:email,:address,:college_id  )
  end

end
