class CoursesController < ApplicationController
  def new
    @course = Course.new
  end

  def index
    @courses = Course.all

  end

  def create
    @course = Course.new(course_params)
    if @course.save
      redirect_to courses_path
    else
      render :action => 'new'
    end
  end
  def edit
    @course = Course.find(params[:id])
  end
  def update
    @course = Course.find(params[:id])
    if @course.update_attributes(course_params)
       redirect_to course_path
    else
       render :action => 'edit'
    end
  end

  def show
    @course = Course.find(params[:id])
  end

  def destroy
    @course = Course.find(params[:id])
    if @course.delete
      redirect_to courses_path
    end

  end

  private

  def course_params
    params.require(:course).permit(:course_name,:course_code,:description  )
  end
end
